//
//  SecondViewController.swift
//  Explore
//
//  Created by Rich Tanner on 10/18/17.
//  Copyright © 2017 Rich Tanner. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var viewLabel: UILabel!
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // now do stuff
        view.backgroundColor = UIColor.purple
        
        count += 1
        
        viewLabel.text = "You have seen this Tab " + String(count) + " times this session"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

