//
//  FirstViewController.swift
//  Explore
//
//  Created by Rich Tanner on 10/18/17.
//  Copyright © 2017 Rich Tanner. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var aniView: UIView!
    @IBOutlet weak var hideyLabel: UILabel!
    
    var midFrame: CGRect!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        view.backgroundColor = UIColor.darkGray
        aniView.backgroundColor = UIColor.lightGray
        aniView.layer.cornerRadius = 25
        
        // set the frame to half the size of our containing view, and center it
        // we then save as our variable and can call it at any point during app lifecycle
        midFrame = CGRect(x: view.frame.width/4, y: view.frame.height/4, width: view.frame.width/2, height: view.frame.height/2)
        aniView.frame = midFrame
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

