//
//  GameDetailsViewController.swift
//  Explore
//
//  Created by Rich Tanner on 10/23/17.
//  Copyright © 2017 Rich Tanner. All rights reserved.
//

import UIKit

class GameDetailsViewController: UIViewController {
    
    @IBOutlet weak var gameImageView: UIImageView!
    @IBOutlet weak var gameTitleLabel: UILabel!
    @IBOutlet weak var gameDescriptionLabel: UILabel!
    @IBOutlet weak var gameYearLabel: UILabel!
    @IBOutlet weak var gamePlayersLabel: UILabel!
    
    var thisGame: GameObject? // the GameObject we get passed in by the TableView

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // We should have been passed ONE copy of a GameObject, so HERE we can set our view outlets to those properties
        
        hookupOurModel()
    }
    
    
    @IBAction func dismissModalSelf(sender: AnyObject) {
        // tell whomever presented us to dismiss us
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func hookupOurModel() {
        
        // when we have "var thisGame: GameObject?" up above, the question mark makes it an "Optional".  The pointer 'thisGame' is allocated either way, and th question mark means "maybe it's of this type"
        // the "if let" below forces the object to be unwrapped and set to the specified type
        
        if let unwrappedGame = thisGame {
            // TODO: CS315 STUDENT - you got the model passed in... not connect it's data to the outlets
            
            NSLog("we got " + unwrappedGame.gameTitle + " game data...")
            
            // TODO: CS315 STUDENT - Be sure to nicely format a label for number of player(s), including max and min
            
        } else {
            NSLog("Crud... no model yet...")
            gameTitleLabel.text = "GameObject not loaded!"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
