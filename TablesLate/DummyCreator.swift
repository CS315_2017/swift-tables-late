//
//  DummyCreator.swift
//  Explore
//
//  Created by Rich Tanner on 10/29/17.
//  Copyright © 2017 Rich Tanner. All rights reserved.
//

import UIKit

class DummyCreator: NSObject {
    
    // MARK: - Make game constants
    // TODO: CS315 STUDENT - Make a total of TEN GameObjects here, and make sure you have images for each of the objects a) INSIDE your Assets.xcassets bundle, and b) ADD the images to your Git Commit/Push!
    let someGame = GameObject(title: "Sample Game Title", gameImage: "mario", releaseYear: 1985, description: " Make sure to add a long description...\n \n With multiple lines../n/n/n And perhaps your REASON for selecting that game!", minPlayer: 2, maxPlayer: 4)
    
    let anotherGame = GameObject(title: "Another Game Title", gameImage: "zelda", releaseYear: 1986, description: " Make sure to add a long description...\n \n With multiple lines.. /n/n/n And perhaps your REASON for selecting that game!", minPlayer: 1, maxPlayer: 1)

    // MARK: - Get them games
    func makeGamesList() -> Array<Array<GameObject>> {
        
        // TODO: CS315 STUDENT - make sure to pass TEN games back via this nested array
        let arrayOne = [someGame, anotherGame]
        let arrayTwo = [anotherGame, someGame, someGame]
        
        let finalArray = [arrayOne, arrayTwo]
        
        return finalArray
    }
}
